FROM docker.io/oven/bun:1.0.13 AS assets

WORKDIR /opt/rps-header/assets

COPY assets/package.json assets/bun.lockb ./
RUN bun install --frozen-lockfile

RUN ln -s /usr/local/bin/bun /usr/local/bin/node
ENV PATH="/opt/rps-header/assets/node_modules/.bin:${PATH}"

COPY assets/ ./

RUN bun run styles:prod


FROM docker.io/library/python:3.11 as app-env

# install git apt package
RUN apt-get update && \
    apt-get install -y git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /opt/rps-header

COPY pyproject.toml poetry.lock ./
RUN pip install poetry==1.7.1 && \
    poetry config virtualenvs.create false && \
    poetry install --no-root --no-directory


FROM app-env as app

COPY app/ app/
RUN poetry install

USER www-data

CMD uvicorn api:app --host '::'


FROM app as final
COPY . .
COPY --from=assets /opt/rps-header/assets/ /opt/rps-header/assets/
