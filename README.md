# RPS Header

The RPS Header is a central navigation component that can be used across multiple web sites and applications to bring a unified navigation to a web portal.

The new version is based on:
- Philosophy: [Hypermedia API](https://htmx.org/essays/hypermedia-apis-vs-data-apis/)
- Backend: 
  - API: FastAPI
  - Authentication: oauth2-proxy
  - Templating: Jinja2
- Frontend:
  - Integration: Minimal vanilla Javascript loader
  - Isolation: [Shaddow DOM](https://developer.mozilla.org/en-US/docs/Web/API/Web_components/Using_shadow_DOM)
  - Framework: ??
  - Icons: Material Desing Icons

## Develop

### boot up your dev environment
```
docker-compose build
docker-compose up
```
go to http://localhost:8020

### add assets
if you want to add frontend assets you can do that in the assets container

enter the assets container with:
```
docker-compose exec assets bash
```

you can then add npm assets with the [bun package manager](https://bun.sh/guides/install/add)
```
bun add tailwindcss
```

### add python libs
if you want to add python libraries you can do that in the app container

enter the app container with:
```
docker-compose exec app bash
```

you can then add python libraries with [poetry](https://python-poetry.org/)
```
poetry add pyjwt
```

after that you need to rebuild the container images with:
```
docker-compose build
```
