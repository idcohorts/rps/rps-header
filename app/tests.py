from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

header_templates = Jinja2Templates(directory="templates")

tests_api = FastAPI()

tests_api.mount("/", StaticFiles(directory="tests", html=True), name="tests")
