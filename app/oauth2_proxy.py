from fastapi import FastAPI, Request, Response
from fastapi.responses import RedirectResponse

import httpx

from jwt import decode, PyJWKClient

oauth2_proxy_url = "http://oauth2-proxy:4180/oauth2"

keycloak_realm_url = "https://accounts.dev.research-project-suite.org/realms/rps"

oidc_config = httpx.get(f"{keycloak_realm_url}/.well-known/openid-configuration").json()
jwks_client = PyJWKClient(oidc_config["jwks_uri"])
signing_algos = oidc_config["id_token_signing_alg_values_supported"]

oauth2_proxy_api = FastAPI()

@oauth2_proxy_api.get("/sign_in")
async def oauth2_sign_in(request: Request):
    oauth2_proxy_response = httpx.get(f"{oauth2_proxy_url}/sign_in",
        headers=request.headers)
    api_response = RedirectResponse(oauth2_proxy_response.headers["Location"], status_code=oauth2_proxy_response.status_code)
    for cookie in oauth2_proxy_response.cookies:
        api_response.set_cookie(cookie, oauth2_proxy_response.cookies[cookie], samesite="lax", secure=True)
    return api_response

@oauth2_proxy_api.get("/callback")
async def oauth2_callback(request: Request):
    oauth2_proxy_response = httpx.get(f"{oauth2_proxy_url}/callback?{request.query_params}",
        headers=request.headers)
    print(oauth2_proxy_response.headers)
    if "Location" in oauth2_proxy_response.headers:
        api_response = RedirectResponse(oauth2_proxy_response.headers["Location"], status_code=oauth2_proxy_response.status_code)
        for request_cookie in request.cookies:
            api_response.delete_cookie(request_cookie)
        for cookie in oauth2_proxy_response.cookies:
            api_response.set_cookie(cookie, oauth2_proxy_response.cookies[cookie], samesite="none", secure=True)
        return api_response
    else:
        return Response(oauth2_proxy_response.content, status_code=oauth2_proxy_response.status_code)

# @oauth2_proxy_api.get("/oauth2/sign_out")
# async def oauth2_sign_out(request: Request):
#     return oauth2_proxy(request, "oauth2/sign_out")

def oauth2_access_token(request: Request):
    oauth2_proxy_response = httpx.get(f"{oauth2_proxy_url}/auth",
        headers=request.headers)
    if oauth2_proxy_response.status_code == 202:
        token = oauth2_proxy_response.headers["X-Auth-Request-Access-Token"]
        signing_key = jwks_client.get_signing_key_from_jwt(token)
        userinfo = decode(token, signing_key.key, algorithms=signing_algos, audience="account")
        return token, userinfo, oauth2_proxy_response.cookies
    else:
        return False, False, []


# @oauth2_proxy_api.get("/userinfo")
# async def oauth2_proxy_userinfo(request: Request):
#     oauth2_proxy_response = httpx.get("http://oauth2-proxy:4180/oauth2/userinfo",
#         headers=request.headers)
#     if oauth2_proxy_response.status_code == 200:
#         return oauth2_proxy_response.json()
#     elif oauth2_proxy_response.status_code == 401:
#         return Response(status_code=401, content="unauthorized")
