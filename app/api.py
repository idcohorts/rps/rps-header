import os
from hashlib import md5

from fastapi import FastAPI, Request, Response
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.templating import Jinja2Templates

from .oauth2_proxy import oauth2_proxy_api, oauth2_access_token

from .config import configFilteredForUser
from .parse_location import parse_location

dev_mode = os.environ.get("DEV_MODE", "true").lower() == "true"

if dev_mode:
    from .tests import tests_api


def CachedResponse(request: Request, response: Response):
    etag = md5(response.body).hexdigest()
    response.headers["ETag"] = etag
    response.headers["Cache-Control"] = "private, no-cache"
    if etag == request.headers.get("If-None-Match"):
        return Response(status_code=304)
    else:
        return response

def CachedFileResponse(request: Request, filename, media_type, **kwargs):
    with open(filename, "rb") as f:
        content = f.read()
    response = Response(content, media_type=media_type, **kwargs)
    return CachedResponse(request, response)

api = FastAPI(
    title="RPS Header",
)

api.add_middleware(GZipMiddleware, minimum_size=500)

header_templates = Jinja2Templates(directory="templates")

class JavascriptResponse(Response):
    media_type = "text/javascript"

class CSSResponse(Response):
    media_type = "text/css"

@api.get("/", response_class=HTMLResponse)
async def index(request: Request):
    context = {
        "request": request,
        "dev_mode": dev_mode,
    }
    return header_templates.TemplateResponse("index.html.jinja", context)

@api.get("/global.css", response_class=CSSResponse)
async def global_css(request: Request):
    return header_templates.TemplateResponse("style/global.css", {"request": request}, media_type="text/css")

@api.get("/header.css", response_class=CSSResponse)
async def header_css(request: Request):
    return header_templates.TemplateResponse("style/header.css", {"request": request}, media_type="text/css")

@api.get("/breadcrumbs.html", response_class= HTMLResponse)
async def breadcrumbs(request: Request, browser_location: str = None):
    token, userinfo, cookies = oauth2_access_token(request)
    context = {
        "request" : request,
        "config": configFilteredForUser(userinfo),
    }
        
    parsed_urls = parse_location(browser_location, token, dev_mode)
    if parsed_urls:
        context["breadcrumbs"] = parsed_urls["breadcrumbs"]
    else:
        context["breadcrumbs"] = []
        
    response = CachedResponse(request, header_templates.TemplateResponse("breadcrumbs.html.jinja", context, media_type="text/html"))
    for cookie in cookies:
        response.set_cookie(cookie, cookies[cookie], samesite="none", secure=True)
    return response

@api.get("/header.js", response_class=JavascriptResponse)
async def header_js(request: Request):
    token, userinfo, cookies = oauth2_access_token(request)
    context = {
        "request": request,
        "config": configFilteredForUser(userinfo),
        "userinfo": userinfo,
        
    }
    response = CachedResponse(request, header_templates.TemplateResponse("header.js.jinja", context, media_type="text/javascript"))
    for cookie in cookies:
        response.set_cookie(cookie, cookies[cookie], samesite="none", secure=True)
    return response

@api.get("/header.html", response_class=HTMLResponse)
async def header_html(request: Request, only_content: bool = False):
    token, userinfo, cookies = oauth2_access_token(request)

    context = {
        "request": request,
        "config": configFilteredForUser(userinfo),
        "userinfo": userinfo,
        "only_content": only_content,
        "file_exists" : file_exists
    }
    
    response = CachedResponse(request, header_templates.TemplateResponse("header.html.jinja", context, media_type="text/html"))
    for cookie in cookies:
        response.set_cookie(cookie, cookies[cookie], samesite="none", secure=True)
    return response

@api.get("/howto", response_class=HTMLResponse)
async def howto(request: Request):
    return header_templates.TemplateResponse("howto.html.jinja", {"request": request})

api.mount("/oauth2", oauth2_proxy_api)

if dev_mode:
    api.mount("/tests", tests_api)

api.mount("/", StaticFiles(directory="public"), name="public")


def file_exists(file_path):
    return os.path.exists('./templates/icons/'+file_path)

