from .config import get_breadcrumb_config
from urllib.parse import urlparse, parse_qs, urlunparse, urlencode
import requests
from dotenv import load_dotenv
import os


load_dotenv()
config = get_breadcrumb_config()


def get_group_by_id(id : str, bearer: str):
    headers = {'Authorization': f"Bearer {bearer}"}
    url= f"{os.getenv('KEYCLOAK_URL')}/admin/realms/{os.getenv('KEYCLOAK_REALM')}/groups/{id}"
    response = requests.get(url, headers=headers)
    
    if response.status_code == 200:
        return response.json()


def get_group_by_path(path : str, bearer: str):
    headers = {'Authorization': f"Bearer {bearer}"}
    url= f"{os.getenv('KEYCLOAK_URL')}/admin/realms/{os.getenv('KEYCLOAK_REALM')}/group-by-path/{path.replace('/', '')}"
    response = requests.get(url, headers=headers)
    
    if response.status_code == 200:
        return response.json()


def parse_location(browser_location: str, bearer: str, dev_mode = False) -> dict:
    # Check if config is valid
    if not config:
        return {'breadcrumbs': []}
    
    if not 'services' in config:
        return {'breadcrumbs':[]}
    
    if not bearer and not dev_mode:
        return {'breadcrumbs':[]}
    
    # Remove HTTPS / HTTP
    
    parsed_url = urlparse(browser_location)
    
    path = parsed_url.path
    
    fragment = parsed_url.fragment
    
    hostname = parsed_url.hostname
    
    query = parsed_url.query
    
    selected_service = None
    breadcrumbs = []
    print(hostname)
    print(config)
    
    # Add dev service configuration
    if dev_mode:
        config['services']['dev'] = {'service_name': 'dev','icon':'bug', 'service_url': 'http://localhost:8020'}
        
    for service_name in config['services'].keys():
        service = config['services'][service_name]
        service_hostname = urlparse(service['service_url']).hostname
        print (service_hostname)
        if service_hostname == hostname:
            selected_service = service_name
            
    if not selected_service:
        return {'breadcrumbs':[]}
    
    match selected_service.lower():
        case 'cloud':
            breadcrumbs = parse_cloud(query)
            breadcrumbs = [selected_service.lower()] + breadcrumbs

            
        case 'chat':
            resource_links = {}
            
            breadcrumbs = parse_chat(fragment, bearer)
            breadcrumbs = [selected_service.lower()] + breadcrumbs
            
            cloud_url = get_cloud_link(breadcrumbs, config['services']['Cloud'])
            resource_links['cloud'] = cloud_url
            
            pass
        case 'dev':
            breadcrumbs = parse_dev(path=path)
            breadcrumbs = [selected_service.lower()] + breadcrumbs
    return  {
            'breadcrumbs' : breadcrumbs,
            #'resource_links': resource_links
            }



def parse_dev(path):
    breadcrumps = path.split('/')[1:]
    return breadcrumps
    print(path)
    pass

def parse_cloud(query : str):
        query_dict = parse_qs(query)
        if 'dir' in query_dict:
            breadcrumb_path = query_dict['dir'][0]
            print(breadcrumb_path)
            breadcrumbs = breadcrumb_path.split('/')[1:]
            return breadcrumbs

def parse_discourse(path : str):
    pass

def parse_kanban(path : str):
    pass

def parse_chat(fragment : str, bearer: str):
    
    fragment = fragment.replace("/room/#", '')
    id = fragment[:fragment.find(':')]
    kc_group = get_group_by_id(id, bearer)
    breadcrumbs = kc_group['path'].split('/')[1:]
    return breadcrumbs

def get_cloud_link(breadcrumbs, service_config):
    query_params = {
        'dir': '/' + '/'.join(breadcrumbs[1:]),
    }

    url = urlunparse((
        'https',
        urlparse(service_config['service_url']).hostname,
        '/apps/files/',
        None,
        urlencode(query_params),
        ''

    ))
    
    return url