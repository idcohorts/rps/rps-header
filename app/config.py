import yaml

def config():
    with open("config.yaml", "r") as f:
        return yaml.safe_load(f)

def configFilteredForUser(userinfo):
    filteredConfig = config()
    filteredConfig["top_navbar_links"] = filterList(filteredConfig["top_navbar_links"], userinfo)
    filteredConfig["bottom_navbar_links"] = filterList(filteredConfig["bottom_navbar_links"], userinfo)
    return filteredConfig

def filterList(list, userinfo):
    groups = userinfo.get("groups", []) if userinfo else []
    filteredList = []
    for item in list:

        # check if login is required for this item
        # and if the user is logged in
        if item.get("requireLogin", False) and not userinfo:
            continue

        # check if any required groups are set
        # and if the user is in any of the groups
      #  if item.get("requiredGroups", False):
      #      if any(item["requiredGroups"]) not in groups:
      #          continue

        ## TODO: requiredRoles
        
        # check for children and filter them
        if "children" in item:
            item["children"] = filterList(item["children"], groups)
            if len(item["children"]) == 0:
                del item["children"]
        filteredList.append(item)
    return filteredList

def get_breadcrumb_config() -> dict | None:
    configuration = config()
    
    if 'breadcrumbs' in configuration:
        return configuration['breadcrumbs']
    return None
